%%%-------------------------------------------------------------------
%%% @doc
%%% @end
%%%-------------------------------------------------------------------
-module(commrelease_srv2).
-behaviour(gen_server).

%% API
-export([start_link/2, send/1]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3, format_status/2]).

%% internal function
-export([do_connect/3, do_send/2]).

%% macros
-define(SERVER, ?MODULE).

%% record to hold the state of the server
-record(state, {host = null :: null | gen_tcp:hosts(),
                port = null :: null | port(),
                socket = null :: null | gen_tcp:socket(),
                connector = null :: null | pid(),
                data :: queue:queue()}).


%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% API-function to handle date which is to be sent. It simply calls
%% call/2 which then is handled by handle_call/3.
%% This function is called by a other server *inside* this system
%% (server 1 in this particular case).
%% @end
%%--------------------------------------------------------------------
send(Data) ->
  gen_server:call(?MODULE, {send, Data}).


%%--------------------------------------------------------------------
%% @doc
%% Starts the server.
%% @end
%%--------------------------------------------------------------------
-spec start_link(Host :: gen_tcp:hosts(),
                 Port :: port()) ->
        {ok, Pid :: pid()} |
        {error, Error :: {already_started, pid()}} |
        {error, Error :: term()} |
        ignore.
start_link(Host, Port) ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [Host, Port], []).


%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Init called when start_link/1 is executed.
%% A listen socket is created and a Worker process is spawned.
%% The worker accepts incoming requests and creates a socket for
%% communication.
%% @end
%%--------------------------------------------------------------------
-spec init(Args :: term()) ->
        {ok, State :: term()} |
        {ok, State :: term(), Timeout :: timeout()} |
        {ok, State :: term(), hibernate} |
        {stop, Reason :: term()} |
        ignore.
init([Host, Port]) ->
  process_flag(trap_exit, true),
  Worker = erlang:spawn_link(commrelease_srv2, do_connect, [Host, Port, self()]),
  {ok, #state{port = Port, host = Host, connector = Worker, data = queue:new()}}.


%%--------------------------------------------------------------------
%% @doc
%% Handle call for synchronous requests:
%%
%% handle_call({set_socket, Sock}, _From, State) sets the socket active.
%%
%% handle_call({send, _}, _From, #state{socket = null} = State) in case
%% there is no socket an error is returned.
%%
%% handle_call({send, Data}, _From, #state{socket = Socket} = State)
%% sends date over the (active) Socket.
%% @end
%%--------------------------------------------------------------------
-spec handle_call(Request :: term(),
                  From :: {pid(), term()},
                  State :: term()) ->
        {reply, Reply :: term(), NewState :: term()} |
        {reply, Reply :: term(), NewState :: term(), Timeout :: timeout()} |
        {reply, Reply :: term(), NewState :: term(), hibernate} |
        {noreply, NewState :: term()} |
        {noreply, NewState :: term(), Timeout :: timeout()} |
        {noreply, NewState :: term(), hibernate} |
        {stop, Reason :: term(), Reply :: term(), NewState :: term()} |
        {stop, Reason :: term(), NewState :: term()}.
handle_call({set_socket, Sock}, _From, #state{data = Data} = State) ->
  %% active socket will send messages when data arrived
  inet:setopts(Sock, [{active, true}, {mode, binary}]),
  {ok, RemData} = do_send(Sock, Data),
  {reply, ok, State#state{socket = Sock, data = RemData}};

handle_call({send, Data}, _From, #state{socket = null, data = OldData} = State) ->
  %% If there is no socket wie need to store the received data.
  {reply, {error, no_socket}, State#state{data = queue:in(Data, OldData)}};

handle_call({send, Data}, _From, #state{socket = Socket, data = OldData} = State) ->
  {ok, RemData} = do_send(Socket, queue:in(Data, OldData)),
  {reply, ok, State#state{data = RemData}}.


%%--------------------------------------------------------------------
%% @doc
%% Handle cast for asynchronous requests. No asynchronous
%% communication, here.
%% @end
%%--------------------------------------------------------------------
-spec handle_cast(Request :: term(), State :: term()) ->
        {noreply, NewState :: term()} |
        {noreply, NewState :: term(), Timeout :: timeout()} |
        {noreply, NewState :: term(), hibernate} |
        {stop, Reason :: term(), NewState :: term()}.
handle_cast(_Request, State) ->
  {noreply, State}.


%%--------------------------------------------------------------------
%% @doc
%% Handle messages not send by cast or call.
%%
%% handle_info({tcp_closed, Sock}, #state{socket = Sock,
%%             listen_socket = ListenSocket} = State) is triggered by
%% the tcp_closed error thrown by gen_tcp:connect/2.
%% @end
%%--------------------------------------------------------------------
-spec handle_info(Info :: timeout() | term(), State :: term()) ->
        {noreply, NewState :: term()} |
        {noreply, NewState :: term(), Timeout :: timeout()} |
        {noreply, NewState :: term(), hibernate} |
        {stop, Reason :: normal | term(), NewState :: term()}.
handle_info({tcp, Sock, Data}, #state{socket = Sock} = State) ->
  commrelease_srv1:send(Data), %% send to other server, it will send to other socket
  {noreply, State};

handle_info({tcp_closed, Sock}, #state{host = Host, port = Port,
                                       socket = Sock} = State) ->
  Worker = erlang:spawn_link(commrelease_srv2, do_connect, [Host, Port, self()]),
  {noreply, State#state{port = Port, host = Host, connector = Worker}};

handle_info({'EXIT', _, normal}, State) ->
  {noreply, State#state{connector = null}}.

%%--------------------------------------------------------------------
%% @doc
%% When termination of server is triggered, Both possibly open sockets
%% are closed.
%% @end
%%--------------------------------------------------------------------
-spec terminate(Reason :: normal | shutdown | {shutdown, term()} | term(),
                State :: term()) ->
        any().
terminate(_Reason, #state{socket = null}) ->
  ok;
terminate(_Reason, #state{socket = S}) ->
  gen_tcp:close(S),
  ok.


%%--------------------------------------------------------------------
%% @doc
%% No action implemented, here.
%% @end
%%--------------------------------------------------------------------
-spec code_change(OldVsn :: term() | {down, term()},
                  State :: term(),
                  Extra :: term()) ->
        {ok, NewState :: term()} |
        {error, Reason :: term()}.
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.


%%--------------------------------------------------------------------
%% @doc
%% No action implemented, here.
%% @end
%%--------------------------------------------------------------------
-spec format_status(Opt :: normal | terminate, Status :: list()) ->
  Status :: term().
format_status(_Opt, Status) ->
  Status.


%%%===================================================================
%%% Internal functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Internal function to connect to remote server. It cycles until it
%% succeeds to connect to remote server.
%% @end
%%--------------------------------------------------------------------
do_connect(Host, Port, Parent) ->
  case  gen_tcp:connect(Host, Port, [binary]) of
    {ok, NewSocket} ->
      % hand over control of socket to parent process
      gen_tcp:controlling_process(NewSocket, Parent),
      % call parent process to set the NewSocket active
      gen_server:call(Parent, {set_socket, NewSocket});
    {error, _} ->
      %% check for shutdown message and cycle
      receive
        stop -> exit(stop)
      after 0 ->
        do_connect(Host, Port, Parent)
      end
  end.


%%--------------------------------------------------------------------
%% @doc
%% Internal function which sends messages over Socket to the remote
%% server. If sending does not succeed, the queued messages stay
%% queued.
%% @end
%%--------------------------------------------------------------------
do_send(Socket, Data) ->
  case queue:out(Data) of
    {{value, Msg}, Rest} ->
      case gen_tcp:send(Socket, Msg) of
        ok ->
          do_send(Socket, Rest);
        {error, _} ->
          {ok, Data}
      end;
    {empty, Data} ->
      {ok, Data}
  end.
