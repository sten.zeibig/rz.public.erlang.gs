%%%-------------------------------------------------------------------
%% @doc commrelease top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(commrelease_sup).

-behaviour(supervisor).

%% Supervisor callbacks
-export([start_link/0]).
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================
-spec start_link() -> {ok, Pid :: pid()} |
                      {error, {already_started, Pid :: pid()}} |
                      {error, {shutdown, term()}} |
                      {error, term()} |
                      ignore.
start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================
-spec init(Args :: term()) ->
            {ok, {SupFlags :: supervisor:sup_flags(),
                  [ChildSpec :: supervisor:child_spec()]}} |
            ignore.
init([]) ->
  io:fwrite("Initializing Comm Supervisor ...~n"),

  % TODO: Error handling.
  {ok, Port1} = application:get_env(port_srv1),
  {ok, Host2} = application:get_env(host_srv2),
  {ok, Port2} = application:get_env(port_srv2),

  SupFlags = #{
    strategy => one_for_one,  % one terminates - one is restarted;
    intensity => 1,           % more than 1 restart in
    period => 5},             % 5 sec. leads to supervisor termination;

  Srv1 = #{                   % First, define server1 child.
    id => commrelease_srv1,   % id of child (internally in supervisor);
    start => {commrelease_srv1, start_link, [Port1]},
    restart => permanent,
    shutdown => 5000,         % shut down after timeout (5 seconds)
    type => worker,
    modules => [commrelease_srv1]},  % name of callback module

  Srv2 = #{                   % Second, define server2 child.
     id => commrelease_srv2,  % id of child (internally in supervisor);
     start => {commrelease_srv2, start_link, [Host2, Port2]},
     restart => permanent,
     shutdown => 5000,
     type => worker,
     modules => [commrelease_srv2]},

  {ok, {SupFlags, [Srv1, Srv2]}}.