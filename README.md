# Communicating Servers
*Getting started with Erlang*

## Background

This repo contains a demo project to get started with Erlang. The diagram
below describes the basic concepts.

```mermaid
graph LR
  Client --> CommServer;
  CommServer --> Client;
  CommServer --> RemoteServer;
  RemoteServer --> CommServer;
```

The **CommServer** is what is implemented in this project. It consists
of two parts: CommServer1 and CommServer2. The processes
in detail:

```mermaid
sequenceDiagram;
participant Client;
participant CommServer1;
participant CommServer2;
participant RemoteServer;
  loop
    CommServer1 ->> CommServer1: listen
  end
  Client ->> CommServer1: connect;
  CommServer1 ->> Client: accept;
  CommServer2 ->> RemoteServer: try to connect;
  RemoteServer ->> CommServer2: accept;
  loop
    Client ->> CommServer1: send messages;
    CommServer1 -->> CommServer2: forward messages;
    CommServer2 ->> RemoteServer: forward messages;
  end
  loop
    RemoteServer ->> CommServer2: send messages;
    CommServer2 -->> CommServer1: forward messages;
    CommServer1 ->> Client: forward messages;
  end
```

This setup is meant to deal with bad connectivity on the side of the
client and the remote server. Thus, if one connection gets lost, the
CommServer tries to establish a new one. As long as one
connection is lost, messages arriving from the other connection are
stored in a queue.

For simplicity there is only one connection in either direction
established.


## Build and Run the Code

Copy [rebar3](https://www.rebar3.org) to your project root directory.
Go to the project root directory and start with:
```
  $ ./rebar3 new release commrelease
```

Then build with:
```
  $ ./rebar3 releas
```

Run:
```
  $ ./commrelease/build/default/rel/commrelease/bin/commrelease console
```


## Testing

1. Run `commrelease`:
```
  $ ./_build/default/rel/commrelease/bin/commrelease console
```

2. Connect via telnet to localhost port 5678:
```
  $ telnet localhost 5678
```

3. Start an Erlang session and listen on localhost port 5679.
```
  $ erl
```
```
  1> {ok, ListenSocket} = gen_tcp:listen(5679, [{active, true}, binary]).
  2> {ok, AcceptSocket} = gen_tcp:accept(ListenSocket).
```
The process is now listening.

4. Go to back to telnet, type any message and hit enter.
```
  hi!
```

5. Go back to the Erlang console, which now is unlocked (because it
accepted the incoming connection vom CommServer2, which received the
forwarded message from CommServer1, which in turn received your message
from telnet) and type:
```
  3> flush().
```

6. Answer from the Erlang console.
```
  4> gen_tcp:send(AcceptSocket, "Hi yourself!").
```

7. Go back to telnet and see if your message came through.


## Resources

### Erlang
This is a usefull Erlang [tutorial](https://learnyousomeerlang.com).
Also helpfull is this [gist](https://gist.github.com/shizz/1055038).
Other (official) documantation on Erlang is
[here](http://erlang.org/doc/reference_manual/users_guide.html)
and [here](http://erlang.org/doc/design_principles/users_guide.html).
Documentation on [gen_tcp](http://erlang.org/doc/man/gen_tcp.html) and
[gen_server](http://erlang.org/doc/man/gen_server.html) is also very
helpful.

How to read in a config file can be found
[here](https://stackoverflow.com/questions/2475270/how-to-read-the-contents-of-a-file-in-erlang)


### Rebar
For using rebar find information [here](http://www.rebar3.org/docs/releases)
and [here](http://erlang.org/doc/design_principles/release_structure.html).


### Erlang with IntelliJ
How to configure IntelliJ for coding Erlang can be found [here](https://www.jetbrains.com/help/idea/getting-started-with-erlang.html#d976282e12).

